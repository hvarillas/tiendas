import time
import scrapy
import json
from scrapy.http.request import Request

class EfeSpider(scrapy.Spider):
	name = "Hiraoka"
	def __init__(self, product='', **kwargs):
		self.product = product
		super().__init__(**kwargs)
	start_urls = ['https://www.hiraoka.com.pe/results.php']

	def parse(self, response):
		self.extract_product(response)


	def start_requests(self): 
		form_data = {'search':self.product}
		yield scrapy.FormRequest('https://www.hiraoka.com.pe/results.php',formdata=form_data)


	def extract_product(self,response):
		links = response.css('div.producto-card-b')
		for x in links:
			sbrand = x.css('div.card-text strong::text').extract_first()
			info = x.css('div.card-text').extract_first()
			if "Oferta" in info:
				sprice = x.css('div.card-text span.card-Oferta::text').extract_first().replace('Oferta ','')
				scondition = 'Oferta'
			else:
				sprice = x.css('div.card-text').extract_first().split('<br>')[-1].split('Normal')[-1].replace('</div>','')
				scondition = "Regular"
				#print(sprice)

			stitle = x.css('div.card-text').extract_first().split('<br>')[:-2]
			stitle.pop(0)
			stitle = ''.join(stitle).replace('\t','').replace('\n','')
			simage = x.css('div.card-img a img').xpath("@src").extract_first()
			if simage != None:
				simage = "http://hiraoka.com.pe/" + simage
			else:
				simage = ''
			data = {
				'title':stitle,
				'brand':sbrand,
				'price':sprice,
				'condition':scondition,
				'image':simage
			}

			print('------------------------------------')
			print('title : '+stitle)
			print('brand : '+sbrand)
			print('condi : '+scondition)
			print('price : '+sprice)
			print('image : '+simage)

		