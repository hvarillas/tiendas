import scrapy
import time
import json
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('no-sandbox')
options.add_argument('disable-gpu')
options.add_argument('disable-dev-shm-usage')



class EfeSpider(scrapy.Spider):
	name = 'Efe'
	def __init__(self, product='lavadora', **kwargs):
		self.product = product
		super().__init__(**kwargs)
		with open('config.json') as js:
			chromedrive = json.load(js)
		self.start_urls = ['https://www.efe.com.pe/webapp/wcs/stores/servlet/SearchDisplay?categoryId=&storeId=10152&catalogId=3074457345616677168&langId=-24&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q&pageView=&beginIndex=0&pageSize=18&searchTerm={}#facet:&productBeginIndex:0&facetLimit:&orderBy:&pageView:grid&minPrice:&maxPrice:&pageSize:&'.format(product)]
		self.driver = webdriver.Chrome(chromedrive['chromedriver'], chrome_options=options)

	def parse(self, response):
		self.driver.get(response.url)
		# iniPage = self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight); var iniPage=document.body.scrollHeight; return iniPage")
		# fin = False
		# while(fin==False):
		# 	endPage = iniPage
		# 	time.sleep(3)
		# 	iniPage = self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight); var iniPage=document.body.scrollHeight; return iniPage")
		# 	if endPage == iniPage:
		# 		fin = True

		re = scrapy.Selector(text=self.driver.page_source)
		self.extract_product(re)

	def extract_product(self, response):
		links = response.css('ul.grid_mode li')
		for x in links:
			stitle = x.css('div.product div.product_info div.product_name a::text').extract_first()
			sbrand = stitle.split(' ')[1]
			old_price = x.css('div.product div.product_info div.product_price span.old_price::text').extract_first() #.replace('\t','').replace('\n','').strip()
			offert_price = x.css('div.product div.product_info div.product_price span.price::text').extract_first().replace('\t','').replace('\n','').strip()
			if old_price == None:
				old_price = '-'
				scondition = 'Regular'
			else:
				scondition = 'Oferta'
			old_price = old_price.replace('\n','').strip()

			simage = x.css('div.product_image div.image a img').xpath("@src").extract_first()
			simage = 'https://www.efe.com.pe'+simage

			print("-----------------------")
			print('title : '+stitle)
			print('Brand : '+sbrand)
			print('condi : '+scondition)
			print('Price : '+offert_price)
			print('image : '+simage)
			
			
			
