from selenium import webdriver
import scrapy
import json

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('no-sandbox')
options.add_argument('disable-gpu')
options.add_argument('disable-dev-shm-usage')

class LinioSpider(scrapy.Spider):
	name = "Linio"
	def __init__(self, product='', **kwargs):
		self.product = product
		super().__init__(**kwargs)

		self.start_urls = []
		with open('config.json') as js:
			chromedriver = json.load(js)

		#GET URL REDIRECT
		self.dpage = webdriver.Chrome(chromedriver['chromedriver'], chrome_options=options)
		self.dpage.get('https://www.linio.com.pe/search?q='+product)
		self.url = self.dpage.current_url

		#GET NUMBER OF PAGE
		self.dpage.get(self.url)
		data = scrapy.Selector(text=self.dpage.page_source)
		fin = int(data.css('ul.pagination li.page-item a.page-link').xpath("@href").extract()[-1].split('=')[-1]) + 1
		print(str(fin))
		for x in range(1,fin):
			self.start_urls.append(self.url+'&page='+str(x))

	def parse(self, response):
		self.dpage.get(response.url)
		res = scrapy.Selector(text=self.dpage.page_source)
		self.extract_product(res)

	def extract_product(self, response):
		links = response.css('div.catalogue-product')
		for x in links:
			stitle = x.css('a').xpath("@title").extract_first()
			offert_price = x.css('span.price-main::text').extract_first().replace('\n','')
			old_price = x.css('span.original-price::text').extract_first()
			if old_price == None:
				old_price = '-'
				scondition = 'Regular'
			else:
				old_price = old_price.replace('\n','')
				scondition = 'Oferta'
			sbrand = 'No especifica'
			simage = 'http:'+x.css('picture img').xpath("@src").extract_first()

			print('--------------------------------------------')
			print('title : '+stitle)
			print('brand : '+sbrand)
			print('condi : '+scondition)
			print('price : '+offert_price)
			print('image : '+simage)
		