from selenium import webdriver
import scrapy
import json


options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('no-sandbox')
options.add_argument('disable-gpu')
options.add_argument('disable-dev-shm-usage')

class RipleySpider(scrapy.Spider):
	name = "Ripley"
	def __init__(self, product='', **kwargs):
		self.product = product
		super().__init__(**kwargs)

		self.start_urls = []

		with open('config.json') as js:
			chromedriver = json.load(js)

		#GET URL REDIRECT
		self.dpage = webdriver.Chrome(chromedriver['chromedriver'], chrome_options=options)
		self.dpage.get('https://simple.ripley.com.pe/search/'+product)
		self.url = self.dpage.current_url

		#GET NUMBER OF PAGE
		self.dpage.get(self.url)
		data = scrapy.Selector(text=self.dpage.page_source)
		fin = int(data.css('ul.pagination li a::text').extract()[-1]) + 1

		for x in range(1,fin):
			self.start_urls.append(self.url+'?page='+str(x))

	def parse(self, response):
		self.dpage.get(response.url)
		res = scrapy.Selector(text=self.dpage.page_source)
		self.extract_product(res)

	def extract_product(self, response):
		links = response.css('a.catalog-product-item')
		for x in links:
			sbrand = x.css('div.catalog-product-details div.catalog-product-details__logo-container div.brand-logo span::text').extract_first()
			stitle = x.css('div.catalog-product-details__name::text').extract_first()

			price_list = x.css('li.catalog-prices__list-price::text').extract_first()
			if price_list == None:
				price_list = '-'

			price_offert = x.css('li.catalog-prices__offer-price::text').extract_first()
			if price_offert == None:
				price_offert = '-'

			price_card = x.css('li.catalog-prices__card-price::text').extract_first()
			if price_card == None:
				price_card = '-'

			if price_list =="-" and price_offert =="-" and price_card=="-":
				scondition = 'Agotado'
			else:
				scondition = 'Online'
			# if price_offer != '-' and price_list =='-':
			# 	scondition = 'Regular'
			# if price_offer != '-' and price_list !='-':
			# 	scondition = 'Oferta'



			simage = 'https:'+x.css('div.images-preview-item img').xpath("@data-src").extract_first()

			print('------------------------------------')
			print('title : '+stitle)
			print('brand : '+sbrand)
			print('condi : '+scondition)
			print('price : '+price_offert)
			print('image : '+simage)