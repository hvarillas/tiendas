import scrapy
from selenium import webdriver
import re
import json

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('no-sandbox')
options.add_argument('disable-gpu')
options.add_argument('disable-dev-shm-usage')

class SagaSpider(scrapy.Spider):
	name = 'Saga'
	def __init__(self, product='caja', **kwargs):
		self.product = product
		super().__init__(**kwargs)
		self.start_urls = []

		with open('config.json') as js:
			chromedriver = json.load(js)

		#GET MAX NUMBER OF PAGE
		self.dpage = webdriver.Chrome(chromedriver['chromedriver'], chrome_options=options)
		self.dpage.get("https://www.falabella.com.pe/falabella-pe/search/?Ntt="+ product)
		data = self.dpage.page_source
		items = re.findall(r'<div class="no-selected-number extreme-number">(.*?)</div>', data)
		try:
			fin = int(items[1]) + 1
		except:
			fin = 2

		#GENERATE URLs
		for x in range(1,fin):
			url = "https://www.falabella.com.pe/falabella-pe/search/?Ntt="+ product +"&page="+str(x)
			self.start_urls.append(url)
		
		self.driver = webdriver.Chrome('E:/chromedriver.exe', chrome_options=options)

	def parse(self, response):
		if response.url == "https://www.falabella.com.pe/falabella-pe/":
			pass
		else:
			self.driver.get(response.url)
			res = scrapy.Selector(text=self.driver.page_source)
			self.extract_product(res)

	def extract_product(self, response):
		result = response.css('h3.fb-category-search__breadcrumb--suggestion::text').extract_first()
		if result == None:
			
			links = response.css('div.pod-item')
			print(len(links))
			for x in links:
				stitle = x.css('div.pod-body a.section__pod-top div.section__pod-top-title div.LinesEllipsis::text').extract_first()
				sbrand = x.css('div.pod-body a.section__pod-top div.section__pod-top-brand::text').extract_first()
				simage = 'http:'+x.css('div.content__image img').xpath("@src").extract_first()
				price = x.css('div.fb-price-list p.fb-price::text').extract()
				inter_price = "-"
				normal_price = "-"
				card_price = "-"
				for x in price:
					if "Internet" in x:
						inter_price = x
					elif "Normal" in x:
						normal_price = x
					else:
						card_price = x

				if inter_price == "-" and normal_price != "-":
					scondition = "Regular"
					sprice = normal_price.replace(' (Normal)','')
				if inter_price !='-':
					scondition = "Online"
					sprice = inter_price.replace(' (Internet)','')

				print('------------------------------------')
				print('title : '+stitle)
				print('brand : '+str(sbrand))
				print('condi : '+scondition)
				print('price : '+sprice)
				print('image : '+simage)
		else:
			print("no hay resultado")